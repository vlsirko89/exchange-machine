CREATE TABLE IF NOT EXISTS `books` (
  `id`    INT(11) PRIMARY KEY AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `authors` (
  `id`   INT(11) PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL
)
  ENGINE = INNODB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `authors_vs_book` (
  `book_id`   INT(11) NOT NULL,
  `author_id` INT(11) NOT NULL,
  PRIMARY KEY (`book_id`, `author_id`),

  FOREIGN KEY `book_fk` (`book_id`) REFERENCES `books` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

  FOREIGN KEY `authors_fk` (`author_id`) REFERENCES `authors` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

#book 1
INSERT INTO `books` (`title`) VALUES ('RESTful Web Services Cookbook');
SET @book_id = LAST_INSERT_ID();

INSERT INTO `authors` (`name`) VALUES ('Джейсон Ленгсторф');
SET @author_id = LAST_INSERT_ID();

INSERT INTO `authors_vs_book` (`book_id`, `author_id`) VALUES (@book_id, @author_id);

#book 2
INSERT INTO `books` (`title`) VALUES ('Multi-Tier Application Programming with PHP');
SET @book_id = LAST_INSERT_ID();

INSERT INTO `authors` (`name`) VALUES ('Максим Кузнецов');
SET @author_id = LAST_INSERT_ID();

INSERT INTO `authors_vs_book` (`book_id`, `author_id`) VALUES (@book_id, @author_id);

#book 3
INSERT INTO `books` (`title`) VALUES ('Объекты, шаблоны и методики программирования');
SET @book_id = LAST_INSERT_ID();

INSERT INTO `authors` (`name`) VALUES ('Мэтт Зандстра');
SET @author_id = LAST_INSERT_ID();

INSERT INTO `authors_vs_book` (`book_id`, `author_id`) VALUES (@book_id, @author_id);

#book 4
INSERT INTO `books` (`title`) VALUES ('Библия программиста'); #book with 3 authors;
SET @book_id = LAST_INSERT_ID();

INSERT INTO `authors` (`name`) VALUES ('Стив Суэринг');
SET @author_id = LAST_INSERT_ID();

INSERT INTO `authors_vs_book` (`book_id`, `author_id`) VALUES (@book_id, @author_id);

INSERT INTO `authors` (`name`) VALUES ('Тим Конверс');
SET @author_id = LAST_INSERT_ID();

INSERT INTO `authors_vs_book` (`book_id`, `author_id`) VALUES (@book_id, @author_id);

INSERT INTO `authors` (`name`) VALUES ('Джойс Парк');
SET @author_id = LAST_INSERT_ID();

INSERT INTO `authors_vs_book` (`book_id`, `author_id`) VALUES (@book_id, @author_id);

#QUERY
SELECT
  `b`.`title`                    AS book_title,
  count(`b`.`id`)                AS authors_count,
  GROUP_CONCAT(`authors`.`name`) AS authors_name
FROM `books` AS `b`
  JOIN `authors_vs_book` ON `b`.`id` = `authors_vs_book`.`book_id`
  JOIN `authors` ON `authors_vs_book`.`author_id` = `authors`.`id`
GROUP BY `b`.`id`
HAVING authors_count = 3;
