<?php

/**
 * Created by PhpStorm.
 * User: sirenko
 * Date: 15.07.16
 * Time: 10:00
 */
class MoneyExchangeMachine
{
    const MAX_SUMM = 10000;

    /**
     * @var CoinStoragePlug
     */
    protected $coinStorage;

    /**
     * @param CoinStoragePlug $coinStorage
     */
    public function setCoinStorage($coinStorage)
    {
        $this->coinStorage = $coinStorage;
    }

    /**
     * @param $sum
     * @return array
     * @throws Exception
     */
    public function exchange($sum)
    {
        if (!$this->validate($sum)) {
            throw new Exception('Input sum is bad');
        }

        $mainFunc = function ($sum, $combination = [], $maxCoinValue = null) use (&$mainFunc) {

            foreach ($this->coinStorage->getAvailableCoinTypes() as $coinValue => $coinStorageQuantity) {

                if ($sum < $coinValue || (!is_null($maxCoinValue) && $coinValue > $maxCoinValue)) {
                    continue;
                }

                if ($sum == $coinValue) {
                    $combination[] = $coinValue;
                    return $combination;
                }

                if ($necessaryCoinCount = floor($sum / $coinValue)) {

                    $coinCount = $coinStorageQuantity >= $necessaryCoinCount ? $necessaryCoinCount : $coinStorageQuantity;
                    $this->coinStorage->deduct($coinValue, $coinCount);

                    for ($i = 0; $i < $coinCount; $i++) {
                        $combination[] = $coinValue;
                    }

                    if ($restedSum = $sum - ($coinCount * $coinValue)) {
                        return $mainFunc($restedSum, $combination, $coinValue);
                    }

                    break;
                }
            }

            return $combination;
        };

        return $mainFunc($sum);

    }

    /**
     * @param $sum
     * @return bool
     */
    protected function validate($sum)
    {
        return $sum > 0 && $sum <= self::MAX_SUMM;
    }
}

/**
 * Class CoinStoragePlug
 * Represents money storage plug for our machine
 */
class CoinStoragePlug
{
    /**
     * @var array
     */
    private $coinTypeQuantityMap = [];

    /**
     * @var array
     */
    protected $coinTypes = [50, 25, 10, 5, 2, 1];

    public function __construct()
    {
        foreach ($this->coinTypes as $type) {
            if (mt_rand(0, 5)) {
                $this->coinTypeQuantityMap[$type] = mt_rand(1, 100);
            } else {
                print("Монеток номиналом {$type} нет" . PHP_EOL);
            }
        }
    }

    /**
     * @return array
     */
    public function getAvailableCoinTypes()
    {
        return $this->coinTypeQuantityMap;
    }

    /**
     * @param $coinType
     * @param $count
     */
    public function deduct($coinType, $count)
    {
        if (isset($this->coinTypeQuantityMap[$coinType])) {


            $this->coinTypeQuantityMap[$coinType] -= $count;

            if($this->coinTypeQuantityMap[$coinType] <= 0) {
                unset($this->coinTypeQuantityMap[$coinType]);
            }
        }

    }
}

/**
 * usage
 */

$value = intval($argv[1]);
$machine = new MoneyExchangeMachine();
$machine->setCoinStorage(new CoinStoragePlug());
$combination = $machine->exchange($value);

if (array_sum($combination) !== $value) {
    print("Не могу выдать сумму {$value}, нет необходимых монет... попробуйте " . array_sum($combination) . PHP_EOL);
}

print (implode(' + ', $combination) . PHP_EOL);
